<?php

namespace AppBundle\Service;

class WeatherView
{
    const HOT_TEMP = 20;

    public function createView($temperature)
    {
        return [
            'temperature' => "{$temperature}°C",    // string
            'hot' => $temperature > self::HOT_TEMP, // boolean
        ];
    }
}
