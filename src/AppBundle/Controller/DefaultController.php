<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\OpenWeatherClient;
use AppBundle\Service\WeatherView;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request, OpenWeatherClient $client, WeatherView $view)
    {
        // Appel webservice
        $temperature = $client->fetchTemperature('Montpellier,fr');

        $result = $view->createView($temperature);

        return $this->render('default/index.html.twig', $result);
    }
}
