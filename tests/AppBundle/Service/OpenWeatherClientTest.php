<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\OpenWeatherClient;
use AppBundle\Service\RestClient;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class OpenWeatherClientTest extends TestCase
{
    public function testFetchTemperature()
    {
        $key = 'abcd1234';

        $rest = $this
                    ->getMockBuilder(RestClient::class)
                    ->setMethods(['getContent'])
                    ->getMock()
        ;

        $rest
            ->expects($this->once())
            ->method('getContent')
            ->will(
                $this->returnValue('{"main":{"temp":8.78,"pressure":1020,"humidity":50,"temp_min":5.15,"temp_max":12.15}}')
            )
        ;

        $client = new OpenWeatherClient($rest, $key);
        $temperature = $client->fetchTemperature('AnyCity');

        $this->assertEquals(8.78, $temperature);
    }
}
